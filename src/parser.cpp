#include <dbusclixx/parser.hpp>
#include <iostream>

using namespace dbusclixx::DBus;

Parser::Parser()
    : m_current( new Value())
{

}

Parser::~Parser()
{

}

bool Parser::addLine(const std::string& line)
{
    auto tags = splitLine(line);

    if(!tags.empty()) {

        if(tags[0] == "array") {
            if(m_current->is_empty()) {
                m_current = std::make_unique<Value>(ValueType::ARRAY);
            } else {

            }
        } else if(tags[0] == "]" || tags[0] == "}" || tags[0] == ")") {

            m_sub_parser.reset();
        } else if(tags[0] == "struct") {

        } else if(tags[0] == "dict") {

        } else if(tags[0] == "variant") {

        } else if(tags[0] == "string") {
            if(m_current->is_container()) {
                m_current->append( new Value(tags[1]) );
            }
        }
    }

    return true;
}

std::vector<std::string> Parser::splitLine(const std::string& line)
{
    std::vector<std::string> result;
    enum class SplitState {
        SPACE,
        WORD,
        STRING
    };
    SplitState state = SplitState::SPACE;

    unsigned idx=0, last=0;
    for(const auto c : line) {
        if(isspace(c)) {
            switch(state) {
                case SplitState::SPACE:
                    // just continue
                    last = idx;
                    break;
                case SplitState::WORD:
                    // finish word
                    //std::cout << "finish word: " << line.substr(last, idx-last+1) << std::endl;
                    result.push_back( line.substr(last, idx-last) );
                    state = SplitState::SPACE;
                    break;
                case SplitState::STRING:
                    // just continue
                    break;
            }
        } else if(c == '"') {
            // STRING
            switch(state) {
                case SplitState::SPACE:
                    state = SplitState::STRING;
                    // start string
                    last = idx;
                    // std::cout << "start string\n";
                    break;
                case SplitState::WORD:
                    // just continue
                    break;
                case SplitState::STRING:
                    // finish string
                    //std::cout << "finish string: " << line.substr(last+1, idx-last-1) << std::endl;
                    result.push_back( line.substr(last+1, idx-last-1) );
                    last = idx;
                    state = SplitState::SPACE;
                    break;
            }
        } else {
            // WORD
            switch(state) {
                case SplitState::SPACE:
                    // start new word
                    last = idx;
                    state = SplitState::WORD;
                    break;
                case SplitState::WORD:
                    // just continue
                    break;
                case SplitState::STRING:
                    // continue
                    break;
            }
        }
        idx++;
    }

    if(state == SplitState::WORD) {
        if(idx-last > 0) {
            result.push_back( line.substr(last, idx-last+1) );
        }
    }

    return result;
}

std::unique_ptr<Value> Parser::getValue() 
{
    return std::move(m_current);
}

//    array [
//      string "org.freedesktop.DBus
//   ]

//      struct {
//      }

//      dict entry(
//      )

//      struct {
//        object path "/net/connman/service/wifi_b827eb883863_466f72746e697465206f6e6c79_managed_psk"
//        array [
//            dict entry(
//            string "Type"
//            variant                   string "wifi"
//            )
//            dict entry(
//            string "Security"
//            variant                   array [
//                    string "psk"
//                    string "wps"
//                ]
//            )
//        ]
//      }

