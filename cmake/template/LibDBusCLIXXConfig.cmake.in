# libdbusclixx-config.cmake
# --------------------
#
# Libdbusclixx cmake module.
# THis module sets the following variables:
#
# ::
#
#   LIBDBUSCLIXX_INCLUDE_DIRS      - Directory where libdbusclixx headers are located.
#   LIBDBUSCLIXX_LIBRARIES         - libdbusclixx libraries to link against.
#   LIBDBUSCLIXX_VERSION_MAJOR     - The major version of libdbusclixx.
#   LIBDBUSCLIXX_VERSION_MINOR     - The minor version of libdbusclixx.
#   LIBDBUSCLIXX_VERSION_PATCH     - The patch version of libdbusclixx.
#   LIBDBUSCLIXX_VERSION_STRING    - version number as a string (ex: "2.3.4")
#   LIBDBUSCLIXX_MODULES           - whether libdbusclixx as dso support

get_filename_component(_libdbusclixx_rootdir ${CMAKE_CURRENT_LIST_DIR}/../../../ ABSOLUTE)

set(LIBDBUSCLIXX_VERSION_MAJOR  @LIBDBUSCLIXX_MAJOR_VERSION@)
set(LIBDBUSCLIXX_VERSION_MINOR  @LIBDBUSCLIXX_MINOR_VERSION@)
set(LIBDBUSCLIXX_VERSION_MICRO  @LIBDBUSCLIXX_MICRO_VERSION@)
set(LIBDBUSCLIXX_VERSION_STRING "@VERSION@")
set(LIBDBUSCLIXX_INSTALL_PREFIX ${_libdbusclixx_rootdir})
set(LIBDBUSCLIXX_INCLUDE_DIRS   ${_libdbusclixx_rootdir}/include ${_libdbusclixx_rootdir}/include/dbusclixx)
set(LIBDBUSCLIXX_LIBRARY_DIR    ${_libdbusclixx_rootdir}/lib)
set(LIBDBUSCLIXX_LIBRARIES      -L${LIBDBUSCLIXX_LIBRARY_DIR} -ldbusclixx -lpthread)

if(@WITH_THREADS@)
  find_package(Threads REQUIRED)
  list(APPEND LIBDBUSCLIXX_LIBRARIES ${CMAKE_THREAD_LIBS_INIT})
endif()

list(APPEND LIBDBUSCLIXX_LIBRARIES @WIN32_EXTRA_LIBADD@ @LIBS@)

# whether libdbusclixx has dso support
#set(LIBDBUSCLIXX_MODULES @WITH_MODULES@)

mark_as_advanced( LIBDBUSCLIXX_INCLUDE_DIRS LIBDBUSCLIXX_LIBRARIES )
