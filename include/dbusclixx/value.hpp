/**
 * MIT License
 * 
 * Copyright (c) 2019 Colibri-Embedded
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef DBUS_REPLY_HPP
#define DBUS_REPLY_HPP

#include <string>
#include <cstdint>
#include <vector>
#include <memory>
#include <type_traits>



namespace dbusclixx {
namespace DBus {

    enum class ValueType {
        UNKNOWN,
        INT16,
        UINT16,
        INT32,
        UINT32,
        INT64,
        UINT64,
        DOUBLE,
        BYTE,
        STRING,
        OBJECT_PATH,
        BOOLEAN,
        ARRAY,
        STRUCT,
        DICT_ENTRY,
        VARIANT
    };

    class Value {
        public:

            Value(ValueType _type=ValueType::UNKNOWN, bool variant=false);
            ~Value();

            Value(const char* value, bool variant=false);
            Value(const std::string& value, bool variant=false);
            Value(const std::string&& value, bool variant=false);
            Value(std::int16_t value, bool variant=false);
            Value(std::int32_t value, bool variant=false);
            Value(std::int64_t value, bool variant=false);
            Value(std::uint16_t value, bool variant=false);
            Value(std::uint32_t value, bool variant=false);
            Value(std::uint64_t value, bool variant=false);
            Value(double value, bool variant=false);
            Value(bool value, bool variant=false);

            void operator= (const char* value);
            void operator= (const std::string& value);
            void operator= (const std::string&& value);
            void operator= (std::int16_t value);
            void operator= (std::int32_t value);
            void operator= (std::int64_t value);
            void operator= (std::uint16_t value);
            void operator= (std::uint32_t value);
            void operator= (std::uint64_t value);
            void operator= (double value);
            void operator= (bool value);

            operator std::int16_t() const;
            operator std::int32_t() const;
            operator std::int64_t() const;
            operator std::uint16_t() const;
            operator std::uint32_t() const;
            operator std::uint64_t() const;
            operator bool() const;

            operator double() const;
            operator std::string() const;

            std::shared_ptr<Value> append(Value* item);
            std::shared_ptr<Value> append(const std::shared_ptr<Value>& item);

            size_t size() const;
            std::shared_ptr<Value> operator [](const size_t index);

            bool is_number();
            bool is_double();
            bool is_string();

            bool is_array();
            bool is_dict();
            bool is_struct();

            bool is_container();

            bool is_empty();

        private:
            ValueType m_type;
            bool m_variant;

            typedef std::vector< std::shared_ptr<Value> > items_t;

            std::string m_string;
            std::uint64_t m_unsigned;
            double m_double;
            items_t m_items;
    };

    class value_iterator_type 
    { 
    public: 
        value_iterator_type(Value& collection, size_t const index) 
            : index(index)
            , collection(collection) 
        { } 

    bool operator!= (value_iterator_type const & other) const 
    { 
        return index != other.index; 
    } 

    std::shared_ptr<Value> const & operator* () const 
    { 
        return collection[index]; 
    } 

    value_iterator_type const & operator++ () 
    { 
        ++index; 
        return *this; 
    } 

    private: 
        size_t   index; 
        Value&   collection; 
    };

    inline value_iterator_type begin(Value& collection) 
    { 
        return value_iterator_type(collection, 0); 
    } 

    inline value_iterator_type end(Value& collection) 
    { 
        return value_iterator_type(collection, collection.size()); 
    } 

} // namespace DBus
} // namespace dbusclixx

#endif /* DBUS_REPLY_HPP */