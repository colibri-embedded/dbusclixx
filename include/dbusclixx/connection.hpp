/**
 * MIT License
 * 
 * Copyright (c) 2019 Colibri-Embedded
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef DBUS_CLIXX_CONNECTION_HPP
#define DBUS_CLIXX_CONNECTION_HPP

#include <string>
#include <memory>
#include <dbusclixx/value.hpp>

namespace dbusclixx {
namespace DBus {

    enum class Bus {
        SESSION,
        SYSTEM
    };

    class Connection {
        public:
            /**
             * 
             * 
             */
            Connection(Bus _bus = Bus::SESSION);
            
            /**
             * 
             * 
             */
            std::unique_ptr<Value> method_call(const std::string& dest, const std::string& path, const std::string& interface, const std::string& method);


            /**
             * 
             * 
             */
            bool signal();

        private:
            Bus m_bus;
            
        protected:
    };

} // namespace DBus
} // namespace dbusclixx

#endif /* DBUS_CONNECTION_HPP */