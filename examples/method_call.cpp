#include <iostream>
#include <dbusclixx/dbus.hpp>

using namespace dbusclixx;

int main() {

	//dbusclixx::method_call("org.freedesktop.DBus", "/org/freedesktop/DBus", "org.freedesktop.DBus", "ListNames");
	// dbusclixx::method_call("org.freedesktop.DBus", "/org/freedesktop/DBus", "org.freedesktop.DBus", "ListNames2");
	DBus::Connection bus(DBus::Bus::SYSTEM);

	auto reply = bus.method_call("org.freedesktop.DBus", "/org/freedesktop/DBus", "org.freedesktop.DBus", "ListNames");

	std::cout << "array.size = " << reply->size() << std::endl;

	for(auto item: *reply) {
		//std::cout << "string = " << (std::string)(item) << std::end;
	}

	return 0;
}