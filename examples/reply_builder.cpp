#include <iostream>
#include <dbusclixx/dbus.hpp>

using namespace dbusclixx;

int main() {
    DBus::Value v("Test");

    // v = "test";
    // v = -56;

    std::cout << "string? " << v.is_string() << std::endl;
    std::cout << "string = " << (std::string)v << std::endl;
    std::cout << "int64 = " << (int64_t)v << std::endl;
    std::cout << "double = " << (double)v << std::endl;

    std::cout << "array.size = " << v.size() << std::endl;

	return 0;
}