#include <dbusclixx/value.hpp>
#include <iostream>

using namespace dbusclixx::DBus;

Value::Value(ValueType _type, bool variant)
    : m_type(_type)
    , m_variant(variant)
    , m_unsigned(0)
    , m_double(0.0)
{

}

Value::Value(const char* value, bool variant)
    : m_type(ValueType::STRING)
    , m_variant(variant)
    , m_string(value)
    , m_unsigned(0)
    , m_double(0.0)
{

}

Value::Value(const std::string& value, bool variant)
    : m_type(ValueType::STRING)
    , m_variant(variant)
    , m_string(value)
    , m_unsigned(0)
    , m_double(0.0)
{

}
Value::Value(const std::string&& value, bool variant)
    : m_type(ValueType::STRING)
    , m_variant(variant)
    , m_string(std::move(value))
    , m_unsigned(0)
    , m_double(0.0)
{

}
Value::Value(std::int16_t value, bool variant)
    : m_type(ValueType::INT16)
    , m_variant(variant)
    , m_unsigned(value)
{

}
Value::Value(std::int32_t value, bool variant)
    : m_type(ValueType::INT32)
    , m_variant(variant)
    , m_unsigned(value)
{

}
Value::Value(std::int64_t value, bool variant)
    : m_type(ValueType::INT64)
    , m_variant(variant)
    , m_unsigned(value)
{

}
Value::Value(std::uint16_t value, bool variant)
    : m_type(ValueType::UINT16)
    , m_variant(variant)
    , m_unsigned(value)
{

}
Value::Value(std::uint32_t value, bool variant)
    : m_type(ValueType::UINT32)
    , m_variant(variant)
    , m_unsigned(value)
{

}
Value::Value(std::uint64_t value, bool variant)
    : m_type(ValueType::UINT64)
    , m_variant(variant)
    , m_unsigned(value)
{

}
Value::Value(double value, bool variant)
    : m_type(ValueType::DOUBLE)
    , m_variant(variant)
    , m_double(value)
{

}
Value::Value(bool value, bool variant)
    : m_type(ValueType::BOOLEAN)
    , m_variant(variant)
    , m_unsigned(value)
{

}

Value::~Value()
{

}

void Value::operator= (const char* value)
{
    m_string = value;
    m_type = ValueType::STRING;
}

void Value::operator= (const std::string& value)
{
    m_string = value;
    m_type = ValueType::STRING;
}

void Value::operator= (const std::string&& value)
{
    m_string = std::move(value);
    m_type = ValueType::STRING;
}

void Value::operator= (std::int16_t value)
{
    m_unsigned = value;
    m_type = ValueType::INT16;
}

void Value::operator= (std::int32_t value)
{
    m_unsigned = value;
    m_type = ValueType::INT32;
}

void Value::operator= (std::int64_t value)
{
    m_unsigned = value;
    m_type = ValueType::INT64;
}

void Value::operator= (std::uint16_t value)
{
    m_unsigned = value;
    m_type = ValueType::UINT16;
}

void Value::operator= (std::uint32_t value)
{
    m_unsigned = value;
    m_type = ValueType::UINT32;
}

void Value::operator= (std::uint64_t value)
{
    m_unsigned = value;
    m_type = ValueType::UINT64;
}

void Value::operator= (double value)
{
    m_unsigned = value;
    m_type = ValueType::INT16;
}

void Value::operator= (bool value)
{
    m_unsigned = value;
    m_type = ValueType::INT16;
}


Value::operator std::int16_t() const
{
    return m_unsigned;
}

Value::operator std::int32_t() const
{
    return m_unsigned;
}

Value::operator std::int64_t() const
{
    return m_unsigned;
}

Value::operator std::uint16_t() const
{
    return m_unsigned;
}

Value::operator std::uint32_t() const
{
    return m_unsigned;
}

Value::operator std::uint64_t() const
{
    return m_unsigned;
}

Value::operator double() const
{
    return m_double;
}

Value::operator bool() const
{
    return m_unsigned;
}

Value::operator std::string() const
{
    return m_string;
}

bool Value::is_string()
{
    return m_type == ValueType::STRING;
}

bool Value::is_array()
{
    return m_type == ValueType::ARRAY;
}

bool Value::is_dict()
{
    return m_type == ValueType::DICT_ENTRY;
}

bool Value::is_struct()
{
    return m_type == ValueType::STRUCT;
}

bool Value::is_number()
{
    return 
        (  m_type == ValueType::INT16
        || m_type == ValueType::INT32
        || m_type == ValueType::INT64
        || m_type == ValueType::UINT16
        || m_type == ValueType::UINT32
        || m_type == ValueType::UINT64
        || m_type == ValueType::DOUBLE
        );
}

bool Value::is_double()
{
    return m_type == ValueType::DOUBLE;
}

bool Value::is_container()
{
    return
        ( m_type == ValueType::ARRAY
        || m_type == ValueType::DICT_ENTRY
        || m_type == ValueType::STRUCT
        );
}

bool Value::is_empty()
{
    return m_type == ValueType::UNKNOWN;
}

std::shared_ptr<Value> Value::operator[](const size_t index)
{
    return m_items[index];
}

size_t Value::size() const
{
    return m_items.size();
}

std::shared_ptr<Value> Value::append(const std::shared_ptr<Value>& item)
{
    m_items.push_back(item);
    return item;
}

std::shared_ptr<Value> Value::append(Value* item)
{
    return append( std::make_shared<Value>(item) );
}