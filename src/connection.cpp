#include <dbusclixx/connection.hpp>
#include <dbusclixx/exceptions.hpp>
#include <dbusclixx/parser.hpp>
#include <pstreams/pstream.h>
#include <iostream>

using namespace dbusclixx::DBus;

Connection::Connection(Bus _bus)
    : m_bus(_bus)
{

}
        
std::unique_ptr<Value> Connection::method_call(const std::string& dest, const std::string& path, const std::string& interface, const std::string& method)
{
    auto reply = std::make_unique<Value>();

    std::vector<std::string> argv;
    std::vector<std::string> errors;
    argv.push_back("dbus-send");
    if(m_bus == Bus::SESSION)
        argv.push_back("--session");
    else
        argv.push_back("--system");
    argv.push_back("--dest="+dest);
    argv.push_back("--type=method_call");
    argv.push_back("--print-reply");
    argv.push_back(path);
    argv.push_back(interface + "." + method);
    redi::pstream ps("dbus-send", argv, redi::pstreambuf::pstdout|redi::pstreambuf::pstderr);
    
    std::string line;
    bool first_line = true;
    Parser ctx;
    while (std::getline(ps.out(), line)) {
        if(first_line) {
            // method return time=1557613869.761700 sender=org.freedesktop.DBus -> destination=:1.147 serial=3 reply_serial=2
            // just skip the first line
            first_line = false;
        } else {
            // std::cout << line << std::endl;
            ctx.addLine(line);
        }
    }
    ps.clear();
    reply = ctx.getValue();

    while (std::getline(ps.err(), line)) {
        if(first_line) {
            first_line = false;

            auto m = line.find_first_of(':');
            auto error_type = line.substr(33, m-33);
            auto error_msg = line.substr(m+2);
            if(error_type == "AccessDenied") {
                throw DBus::Error::AccessDenied(error_msg);
            } else if(error_type == "ServiceUnknown") {
                throw DBus::Error::ServiceUnknown(error_msg);
            } else if(error_type == "NoReply") {
                throw DBus::Error::NoReply(error_msg);
            } else if(error_type == "MatchRuleInvalid") {
                throw DBus::Error::MatchRuleInvalid(error_msg);
            } else if(error_type == "UnknownMethod") {
                throw DBus::Error::UnknownMethod(error_msg);
            } else if(error_type == "UnknownObject") {
                throw DBus::Error::UnknownObject(error_msg);
            }

            throw DBus::Error::BusError(error_msg);
        }
    }
    ps.clear();

    return reply;
}

bool Connection::signal()
{
    return true;
}
