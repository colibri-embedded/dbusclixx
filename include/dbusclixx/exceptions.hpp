/**
 * MIT License
 * 
 * Copyright (c) 2019 Colibri-Embedded
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef DBUS_EXCEPTIONS_HPP
#define DBUS_EXCEPTIONS_HPP

#include <exception>
#include <stdexcept>

namespace dbusclixx {
namespace DBus {
namespace Error {

    class BusError : public std::runtime_error
    {
    public:
        /**
         * @brief .
         */
        BusError(const std::string& whatArg)
            : std::runtime_error(whatArg)
        {
        }
    };

    class AccessDenied : public BusError 
    {
        public:
            /**
             * @brief Thrown if a message is denied due to a security policy
             */
            AccessDenied(const std::string& whatArg)
                : BusError(whatArg)
                {}
    };

    class MatchRuleInvalid : public BusError 
    {
        public:
            /**
             * @brief Thrown if the match rule is invalid
             */
            MatchRuleInvalid(const std::string& whatArg)
                : BusError(whatArg)
                {}
    };

    class NoReply : public BusError 
    {
        public:
            /**
             * @brief Thrown if there is no reply to a method call
             */
            NoReply(const std::string& whatArg)
                : BusError(whatArg)
                {}
    };

    class ServiceUnknown : public BusError 
    {
        public:
            /**
             * @brief Thrown if the requested service was not available
             */
            ServiceUnknown(const std::string& whatArg)
                : BusError(whatArg)
                {}
    };

    class UnknownMethod : public BusError 
    {
        public:
            /**
             * @brief Thrown if the method called was unknown on the remote object
             */
            UnknownMethod(const std::string& whatArg)
                : BusError(whatArg)
                {}
    };

    class UnknownObject : public BusError 
    {
        public:
            /**
             * @brief Thrown if the object was unknown on a remote connection
             */
            UnknownObject(const std::string& whatArg)
                : BusError(whatArg)
                {}
    };

} // namespace Error
} // namespace DBus
} // namespace dbusclixx

#endif /* DBUS_EXCEPTIONS_HPP */